﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P04_T6.DAL;
using WEB2021Apr_P04_T6.Models;

namespace WEB2021Apr_P04_T6.Controllers
{
    public class ViewProfileController : Controller
    {
        private CompetitionDAL competitionContext = new CompetitionDAL();
        private CompetitorDAL competitorContext = new CompetitorDAL();


        public ActionResult Create()
        {
            ViewData["Salutation"] = GetSalutations();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Competitor Competitor)
        {
            ViewData["Salutation"] = GetSalutations();
            if (ModelState.IsValid)
            {
                //Add criteria record to database
                Competitor.CompetitorID = competitorContext.Add(Competitor);
                //Redirect user to Judge/Index view
                TempData["CompetitorMessage"] = "Competitor Profile successfully created";
                return RedirectToAction("Create");
            }
            return View();
        }

        public List<SelectListItem> GetSalutations()
        {
            List<SelectListItem> salutations = new List<SelectListItem>();
            salutations.Add(new SelectListItem
            {
                Value = "Mr",
                Text = "Mr"
            });
            salutations.Add(new SelectListItem
            {
                Value = "Ms",
                Text = "Ms"
            });
            salutations.Add(new SelectListItem
            {
                Value = "Mrs",
                Text = "Mrs"
            });
            salutations.Add(new SelectListItem
            {
                Value = "Mdm",
                Text = "Mdm"
            });
            salutations.Add(new SelectListItem
            {
                Value = "Dr",
                Text = "Dr"
            });
            return salutations;
        }
    }
}
