﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P04_T6.DAL;
using WEB2021Apr_P04_T6.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Data.SqlClient;

namespace WEB2021Apr_P04_T6.Controllers
{
    public class AreaInterestController : Controller
    {
        private AreaInterestDAL areaInterestContext = new AreaInterestDAL();

        // GET: AreaInterestController
        public ActionResult Index()
        {
            List<AreaInterest> areaInterestList = areaInterestContext.GetAllAreaInterests();
            return View(areaInterestList);
        }

        // GET: AreaInterestController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AreaInterestController/Create
        public ActionResult Create(int id)
        {
            // Stop accessing the action if not logged in
            // or account not in the admin role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Administrator"))
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        // POST: AreaInterestController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AreaInterest areaInterest)
        {
            if (ModelState.IsValid)
            {
                //Add aoi record to database
                areaInterest.AreaInterestID = areaInterestContext.Add(areaInterest);
                //Redirect user to index view
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(areaInterest);
            }
        }

        // GET: AreaInterestController/Edit/5
        public ActionResult Edit(int id)
        {
            // Stop accessing the action if not logged in
            // or account not in the admin role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Administrator"))
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        // POST: AreaInterestController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: AreaInterestController/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                //Return to listing page, not allowed to delete
                return RedirectToAction("Index");
            }
            AreaInterest aoi = areaInterestContext.GetDetails(id.Value);
            if (aoi == null)
            {
                //Return to listing page, not allowed to delete
                return RedirectToAction("Index");
            }
            return View(aoi);
        }

        // POST: AreaInterestController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(AreaInterest aoi)
        {
            if (!areaInterestContext.GetAllCompetitions(aoi.AreaInterestID).Any())
            {
                // competition doesn't exist, then
                // delete the aoi record from database
                areaInterestContext.Delete(aoi.AreaInterestID);
            }
            return RedirectToAction("Index");
        }
    }
}
