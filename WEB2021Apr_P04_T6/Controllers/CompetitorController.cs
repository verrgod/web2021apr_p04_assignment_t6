﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P04_T6.DAL;
using WEB2021Apr_P04_T6.Models;

namespace WEB2021Apr_P04_T6.Controllers
{
    public class CompetitorController : Controller
    {
        private CompetitionDAL competitionContext = new CompetitionDAL();
        private CompetitorDAL competitorContext = new CompetitorDAL();


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CompetitorMain(int id)
        {
            var loginid = HttpContext.Session.GetString("LoginID");
            int competitorId = competitorContext.GetCompetitorId(loginid);
            List<CompetitionViewModel> competitionList = competitorContext.GetAllCompetitionList(competitorId);
            return View(competitionList);

        }

        public ActionResult Join(int id)
        {
            var loginid = HttpContext.Session.GetString("LoginID");
            int competitorId = competitorContext.GetCompetitorId(loginid);
            if (competitorId != 0)
            {
                int VoteCount = 0;
                int competition = competitorContext.Join(id, competitorId, VoteCount);
            }
            return RedirectToAction("CompetitorMain", "Competitor");

        }

        public IActionResult uploadFile()
        {
            var loginid = HttpContext.Session.GetString("LoginID");
            int competitorId = competitorContext.GetCompetitorId(loginid);
            List<CompetitionViewModel> competitionList = competitorContext.GetAllCompetitionList(competitorId);
            return View(competitionList.Where(x => x.CompetitorID != 0).ToList());
        }
        [HttpPost]
        public IActionResult upload(List<IFormFile> postedFiles, int CompetitionID, int CompetitorID)
        {
            foreach (IFormFile postedFile in postedFiles)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                string type = postedFile.ContentType;
                byte[] bytes = null;
                using (MemoryStream ms = new MemoryStream())
                {
                    postedFile.CopyTo(ms);
                    bytes = ms.ToArray();
                }
                bool update = competitorContext.uploadfile(CompetitionID, CompetitorID, fileName);

            }

            return RedirectToAction("uploadFile", "Competitor");
        }

        public IActionResult Score()
        {
            var loginid = HttpContext.Session.GetString("LoginID");
            int competitorId = competitorContext.GetCompetitorId(loginid);
            List<CompetitionScoreViewModel> competitionList = competitorContext.GetAllCompetitionScoreList(competitorId);
            return View(competitionList);
        }


        public ActionResult Edit(int id)
        {
            return View();
        }

        public ActionResult Delete(int id)
        {
            return View();
        }
    }

}
