﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P04_T6.Models;
using WEB2021Apr_P04_T6.DAL;
using Microsoft.AspNetCore.Authentication.Cookies;
using Google.Apis.Auth.OAuth2;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Google.Apis.Auth;
using static Google.Apis.Auth.GoogleJsonWebSignature;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;

namespace WEB2021Apr_P04_T6.Controllers
{
    public class HomeController : Controller
    {
        private JudgeDAL judgeContext = new JudgeDAL();
        private readonly ILogger<HomeController> _logger;
        private CompetitorDAL competitorContext = new CompetitorDAL();

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AdminLogin(IFormCollection formData)
        {
            // Read inputs from textboxes
            // Email address converted to lowercase
            string loginID = formData["txtLoginID"].ToString().ToLower();
            string password = formData["txtPassword"].ToString();
            if (loginID == "admin1@lcu.edu.sg" && password == "p@55Admin")
            {
                // Store Login ID in session with the key “LoginID”
                HttpContext.Session.SetString("LoginID", loginID);
                // Store user role “Administrator” as a string in session with the key “Role”
                HttpContext.Session.SetString("Role", "Administrator");
                // Redirect user to the "AdminMain" view through an action
                return RedirectToAction("AdminMain");
            }
            else
            {
                // Store an error message in TempData for display at the index view
                TempData["AdministratorMessage"] = "Invalid Login Credentials!";
                // Redirect user back to the index view through an action
                return RedirectToAction("Index");
            }
        }
        public ActionResult AdminMain()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CompetitorLogin(IFormCollection formData)
        {
            // Read inputs from textboxes
            // Email address converted to lowercase
            string loginID = formData["txtLoginID"].ToString();
            string password = formData["txtPassword"].ToString();
            bool checkUser = competitorContext.IsUser(loginID, password);

            if (checkUser)
            {
                // Store Login ID in session with the key “LoginID”
                HttpContext.Session.SetString("LoginID", loginID);
                // Store user role “Competitor” as a string in session with the key “Role”
                HttpContext.Session.SetString("Role", "Competitor");
                // Redirect user to the "CompetitorMain" view through an action
                return RedirectToAction("CompetitorMain", "Competitor");
            }
            else
            {
                // Store an error message in TempData for display at the index view
                TempData["CompetitorMessage"] = "Invalid Login Credentials!";
                // Redirect user back to the index view through an action
                return RedirectToAction
                    ("Index");
            }
        }
        public ActionResult CompetitorMain()
        {
            return View();
        }

        [HttpPost]
        public ActionResult JudgeLogin(IFormCollection formData)
        {
            // Read inputs from textboxes
            string loginID = formData["txtLoginID"].ToString();
            string password = formData["txtPassword"].ToString();
            foreach (Judge judge in judgeContext.GetAllJudges())
            {
                if (loginID == judge.Email && password == judge.Password)
                {
                    // Store Login ID in session with the key “LoginID”
                    HttpContext.Session.SetString("LoginID", loginID);
                    // Store user role “Judge” as a string in session with the key “Role”
                    HttpContext.Session.SetString("Role", "Judge");
                    // Storing Judge AOI as a integer with the key "AOI"
                    HttpContext.Session.SetInt32("AOI", judge.AOI);
                    // Storing Judge ID as a integer with the key "JudgeID"
                    HttpContext.Session.SetInt32("JudgeID", judge.JudgeId);
                    // Redirect user to the "JudgeMain" view through an action
                    return RedirectToAction("JudgeMain");
                }
            }
            // Store an error message in TempData for display at the index view
            TempData["JudgeMessage"] = "Invalid Login Credentials!";
            // Redirect user back to the index view through an action
            return RedirectToAction("Index");
        }
        public ActionResult JudgeMain()
        {
            return View();
        }

        [Authorize]
        public async Task<ActionResult> GuestLogin()
        {
            // The user is already authenticated, so this call won't
            // trigger login, but it allows us to access token related values.
            AuthenticateResult auth = await HttpContext.AuthenticateAsync();
            string idToken = auth.Properties.GetTokenValue(
             OpenIdConnectParameterNames.IdToken);
            try
            {
                // Verify the current user logging in with Google server
                // if the ID is invalid, an exception is thrown
                Payload currentUser = await
                GoogleJsonWebSignature.ValidateAsync(idToken);
                string userName = currentUser.Name;
                string eMail = currentUser.Email;
                HttpContext.Session.SetString("LoginID", userName + " / "
                + eMail);
                HttpContext.Session.SetString("Role", "Guest");
                HttpContext.Session.SetString("LoggedInTime",
                 DateTime.Now.ToString());
                return RedirectToAction("GuestMain", "Home");
            }
            catch
            {
                // Token ID is may be tempered with, force user to logout
                return RedirectToAction("LogOut");
            }
        }
        public ActionResult GuestMain()
        {
            return View();
        }

        public ActionResult LogOut()
        {
            // Clear all key-values pairs stored in session state
            HttpContext.Session.Clear();
            // Call the Index action of Home controller
            return RedirectToAction("Index");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
