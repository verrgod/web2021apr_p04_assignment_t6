﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P04_T6.DAL;
using WEB2021Apr_P04_T6.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WEB2021Apr_P04_T6.Controllers
{
    public class JudgeController : Controller
    {
        private AreaInterestDAL areaInterestContext = new AreaInterestDAL();
        private JudgeDAL judgeContext = new JudgeDAL();
        // GET: JudgeController
        public ActionResult Index()
        {
            return View();
        }

        // GET: JudgeController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: JudgeController/Create
        public ActionResult Create()
        {
            // Stop accessing the action if not logged in
// or account not in the "Staff" role
if ((HttpContext.Session.GetString("Role") == null) ||
(HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            //Get all area of interest and salutations
            ViewData["AOI"] = GetAreaInterests();
            ViewData["Salutation"] = GetSalutations();
            return View();
        }
        private List<SelectListItem> GetAreaInterests()
        {
            List<SelectListItem> aoi = new List<SelectListItem>();
            foreach (AreaInterest areaInterest in areaInterestContext.GetAllAreaInterests())
            {
                aoi.Add(new SelectListItem
                {
                    Value = areaInterest.AreaInterestID.ToString(),
                    Text = areaInterest.Name
                });
            }
            return aoi;
        }

        //Salutation List
        public List<SelectListItem> GetSalutations()
        {
            List<SelectListItem> salutations = new List<SelectListItem>();
            salutations.Add(new SelectListItem
            {
                Value = "Mr",
                Text = "Mr"
            });
            salutations.Add(new SelectListItem
            {
                Value = "Ms",
                Text = "Ms"
            });
            salutations.Add(new SelectListItem
            {
                Value = "Mrs",
                Text = "Mrs"
            });
            salutations.Add(new SelectListItem
            {
                Value = "Mdm",
                Text = "Mdm"
            });
            salutations.Add(new SelectListItem
            {
                Value = "Dr",
                Text = "Dr"
            });
            return salutations;
        }

        // POST: JudgeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Judge judge)
        {
            //in case of the need to return to Create.cshtml view
            ViewData["AOI"] = GetAreaInterests();
            ViewData["Salutation"] = GetSalutations();
            if (ModelState.IsValid)
            {
                //Add criteria record to database
                judge.JudgeId = judgeContext.Add(judge);
                //Redirect user to Judge/Index view
                TempData["JudgeErrorMessage"] = "Judge Profile successfully created";
                return RedirectToAction("Create");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                TempData["JudgeErrorMessage"] = "Invalid input, please try again";
                return View(judge);
            }
        }

        // GET: JudgeController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: JudgeController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: JudgeController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: JudgeController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
