﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P04_T6.DAL;
using WEB2021Apr_P04_T6.Models;

namespace WEB2021Apr_P04_T6.Controllers
{
    public class CompetitionScoreController : Controller
    {
        private CompetitionScoreDAL competitionScoreContext = new CompetitionScoreDAL();
        private CompetitionDAL competitionContext = new CompetitionDAL();
        private CriteriaDAL criteriaContext = new CriteriaDAL();

        // GET: CompetitionScore
        public ActionResult Index(int? id, int? compid)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            SubmissionScoreViewModel competitionScoreViewModel = new SubmissionScoreViewModel();
            competitionScoreViewModel.submissionList = competitionScoreContext.GetCompetitionSubmissions();
            if (id != null)
            {
                ViewData["selectedCompetitor"] = id.Value;
                ViewData["selectedCompetition"] = compid.Value;
                // Get list of scores for competitor
                competitionScoreViewModel.scoreList = competitionScoreContext.GetCompetitorScores(compid.Value, id.Value);
            }
            else
            {
                ViewData["selectedCompetitor"] = "";
            }
            return View(competitionScoreViewModel);
        }

        private List<CompetitionSubmission> GetCompetitionSubmissions()
        {
            List<CompetitionSubmission> rCompetitionSubmissionsList = new List<CompetitionSubmission>();
            foreach (CompetitionSubmission comSub in competitionScoreContext.GetCompetitionSubmissions())
            {
                foreach (CompetitionJudge cJudge in GetRelatedCompetitions())
                {
                    if (comSub.CompetitionID == cJudge.CompetitionID)
                    {
                        rCompetitionSubmissionsList.Add(comSub);
                    }
                }
            }
            return rCompetitionSubmissionsList;
        }
        private List<CompetitionJudge> GetRelatedCompetitions()
        {
            List<CompetitionJudge> rCompetitionsList = new List<CompetitionJudge>();
            foreach (CompetitionJudge cJudge in competitionContext.GetCompetitionJudges())
            {
                if (cJudge.JudgeID == HttpContext.Session.GetInt32("JudgeID"))
                {
                    rCompetitionsList.Add(cJudge);
                }
            }
            return rCompetitionsList;
        }

        // GET: CompetitionScore/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CompetitionScore/Create
        public ActionResult Create(string id, string comp)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            CompetitionScore competitionScore = new CompetitionScore(Int32.Parse(comp), Int32.Parse(id));
            ViewData["CriteriaID"] = GetRemainingCriterias(Int32.Parse(comp), Int32.Parse(id));
            return View(competitionScore);
        }

        // POST: CompetitionScore/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CompetitionScore competitionScore)
        {
            if (competitionContext.GetDetails(competitionScore.CompetitionID).ResultReleasedDate < DateTime.Now)
            {
                TempData["CompScoreCreateErrorMessage"] = "Results have already been released for this competition!";
                return View(competitionScore);
            }
            else
            {
                //in case of the need to return to Create.cshtml view
                //ViewData["CompetitionID"] = GetCompetitions();
                if (ModelState.IsValid)
                {
                    //Add competition score record to database
                    int count = competitionScoreContext.Add(competitionScore);
                    //Redirect user to CompetitionScore/Index view
                    return RedirectToAction("Index");
                }
                else
                {
                    //Input validation fails, return to the Create view
                    //to display error message
                    return View(competitionScore);
                }
            }
        }
        private List<SelectListItem> GetRemainingCriterias(int comp, int comid)
        {
            List<CompetitionScore> competitionScores = competitionScoreContext.GetCompetitorScores(comp, comid);
            List<SelectListItem> remainingCriterias = new List<SelectListItem>();
            foreach (CompetitionScore cs in competitionScores)
            {
                bool t = true;
                foreach (Criteria cri in GetCriterias(comp))
                {
                    if (cs.CriteriaID == cri.CriteriaID)
                    {
                        t = false;
                    }
                }
                if (t == true)
                {
                    remainingCriterias.Add(new SelectListItem
                    {
                        Value = cs.CriteriaID.ToString(),
                        Text = cs.CriteriaID.ToString()
                    });
                }
            }
            return remainingCriterias;
        }

        private List<Criteria> GetCriterias(int competitionID)
        {
            List<Criteria> criterias = new List<Criteria>();
            foreach (Criteria criteria in criteriaContext.GetAllCriteria())
            {
                if (criteria.CompetitionID == competitionID)
                {
                    criterias.Add(new Criteria
                    {
                        CriteriaID = criteria.CriteriaID,
                        CriteriaName = criteria.CriteriaName
                    });
                }
            }
            return criterias;
        }

        // GET: CompetitionScore/Edit/5
        public ActionResult Edit(int? critid, int? compid, int? comid)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (critid == null)
            { //Query string parameter not provided
              //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            CompetitionScore competitionScore = competitionScoreContext.GetCompetitorScoreDetails(compid.Value, comid.Value, critid.Value);
            if (competitionScore == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(competitionScore);
        }

        // POST: CompetitionScore/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CompetitionScore competitionScore)
        {
            if (competitionContext.GetDetails(competitionScore.CompetitionID).ResultReleasedDate < DateTime.Now)
            {
                TempData["CompScoreEditErrorMessage"] = "Results have already been released for this competition!";
                return View(competitionScore);
            }
            else
            {
                if (ModelState.IsValid)
                {
                    //Update competition score record to database
                    competitionScoreContext.Update(competitionScore);
                    return RedirectToAction("Index");
                }
                else
                {
                    //Input validation fails, return to the view
                    //to display error message
                    return View(competitionScore);
                }
            }
        }

        // GET: CompetitionScore/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CompetitionScore/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
