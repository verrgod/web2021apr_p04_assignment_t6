﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P04_T6.DAL;
using WEB2021Apr_P04_T6.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WEB2021Apr_P04_T6.Controllers
{
    public class CriteriaController : Controller
    {
        private CriteriaDAL criteriaContext = new CriteriaDAL();
        private CompetitionDAL competitionContext = new CompetitionDAL();

        // GET: CriteriaController
        public ActionResult Index()
        {
            List<Criteria> criteriaList = GetRelatedCriteria();
            return View(criteriaList);
        }

        // GET: CriteriaController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CriteriaController/Create
        public ActionResult Create()
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            ViewData["CompetitionID"] = GetCompetitions();
            return View();
        }
        private List<SelectListItem> GetCompetitions()
        {
            List<SelectListItem> competition = new List<SelectListItem>();
            foreach (Competition comp in competitionContext.GetAllCompetitions())
            {
                if(comp.AreaInterestID == HttpContext.Session.GetInt32("AOI"))
                {
                    competition.Add(new SelectListItem
                    {
                        Value = comp.CompetitionID.ToString(),
                        Text = comp.CompetitionName
                    });
                }
            }
            return competition;
        }
        private List<Criteria> GetRelatedCriteria()
        {
            List<Criteria> criteriaList = new List<Criteria>();
            foreach (Criteria crit in criteriaContext.GetAllCriteria())
            {
                foreach (CompetitionJudge cJudge in GetRelatedCompetitions())
                {
                    if (crit.CompetitionID == cJudge.CompetitionID)
                    {
                        criteriaList.Add(crit);
                    }
                }
            }
            return criteriaList;
        }

        private List<CompetitionJudge> GetRelatedCompetitions()
        {
            List<CompetitionJudge> rCompetitionsList = new List<CompetitionJudge>();
            foreach (CompetitionJudge cJudge in competitionContext.GetCompetitionJudges())
            {
                if (cJudge.JudgeID == HttpContext.Session.GetInt32("JudgeID"))
                {
                    rCompetitionsList.Add(cJudge);
                }
            }
            return rCompetitionsList;
        }

        private List<Competition> GetAllCompetitions()
        {
            // Get a list of branches from database
            List<Competition> competitionList = competitionContext.GetAllCompetitions();
            // Adding a select prompt at the first row of the branch list
            competitionList.Insert(0, new Competition
            {
                CompetitionID = 0,
                CompetitionName = "--Select--"
            });
            return competitionList;
        }
        // POST: CriteriaController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Criteria criteria)
        {
            //in case of the need to return to Create.cshtml view
            ViewData["CompetitionID"] = GetCompetitions();
            if (ModelState.IsValid)
            {
                //Add criteria record to database
                criteria.CriteriaID = criteriaContext.Add(criteria);
                //Redirect user to Criteria/Index view
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(criteria);
            }
        }

        // GET: CriteriaController/Edit/5
        public ActionResult Edit(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            { //Query string parameter not provided
              //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            //ViewData["CompetitionList"] = GetAllCompetitions();
            Criteria criteria = criteriaContext.GetDetails(id.Value);
            if (criteria == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(criteria);
        }

        // POST: CriteriaController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Criteria criteria)
        {
            //ViewData["CompetitionList"] = GetAllCompetitions();
            if (ModelState.IsValid)
            {
                //Update criteria record to database
                criteriaContext.Update(criteria);
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the view
                //to display error message
                return View(criteria);
            }
        }

        // GET: CriteriaController/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            Criteria criteria = criteriaContext.GetDetails(id.Value);
            if (criteria == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(criteria);
        }

        // POST: CriteriaController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Criteria criteria)
        {
            // Delete the criteria record from database
            if (criteriaContext.Delete(criteria.CriteriaID, criteria.CompetitionID) == 0)
            {
                TempData["CriteriaDeleteMessage"] = "Criteria for this Competition has already been used for Scoring.";
                return RedirectToAction("Delete");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
    }
}
