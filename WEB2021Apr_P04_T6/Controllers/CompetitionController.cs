﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P04_T6.DAL;
using WEB2021Apr_P04_T6.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WEB2021Apr_P04_T6.Controllers
{
    public class CompetitionController : Controller
    {
        private Competition competition1 = new Competition();
        private CompetitionDAL competitionContext = new CompetitionDAL();
        private AreaInterestDAL areaInterestContext = new AreaInterestDAL();
        private JudgeDAL judgeContext = new JudgeDAL();
        // GET: AreaInterestController
        public ActionResult Index()
        {
            List<Competition> competitionList = competitionContext.GetAllCompetitions();
            return View(competitionList);
        }

        // GET: AreaInterestController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AreaInterestController/Create
        public ActionResult Create()
        {
            // Stop accessing the action if not logged in
            // or account not in the admin role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Administrator"))
            {
                return RedirectToAction("Index", "Home");
            }

            ViewData["AreaInterestList"] = GetAreaInterests();
            competition1.Judges = GetJudges();
            return View(competition1);
        }
        // POST: AreaInterestController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Competition competition)
        {
            ViewData["AreaInterestList"] = GetAreaInterests();
            if (ModelState.IsValid)
            {
                // Add competition record to database
                competition.CompetitionID = competitionContext.AddCompetition(competition);
                competition1.Judges = GetJudges();
                // get competitionjudge list to pass values from judges selected from create competition page
                List<CompetitionJudge> compJudgeList = new List<CompetitionJudge>();
                foreach (Judges j in competition1.Judges)
                {
                    if (j.Selected == true)
                    {
                        // judges that were ticked on the checkbox
                        // add them into competitionjudge list
                        compJudgeList.Add(new CompetitionJudge
                        {
                            // pass values in
                            CompetitionID = competition.CompetitionID
                            // i could not find a way to pass judgeID into the list, which ultimately made adding records not work
                        });
                    }
                }
                // loop through each competitionjudge
                foreach (CompetitionJudge cj in compJudgeList)
                {
                    // add into database
                    competition.CompetitionID = competitionContext.AddJudge(cj);
                }
                // Redirect user to Index view
                return RedirectToAction("Index");
            }
            else
            {
                // Input validation fails, return to the Create view
                // to display error message
                return View(competition);
            }
        }
        // GET: AreaInterestController/Edit/5
        public ActionResult Edit(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the admin role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Administrator"))
            {
                return RedirectToAction("Index", "Home");
            }

            if (id == null)
            { //Query string parameter not provided
              //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }

            ViewData["AreaInterestList"] = GetAllAreaInterests();
            Competition competition = competitionContext.GetDetails(id.Value);
            if (competition == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(competition);
        }

        // POST: AreaInterestController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Competition competition)
        {
            ViewData["AreaInterestList"] = GetAllAreaInterests();
            if (ModelState.IsValid)
            {
                if (!competitionContext.GetCompetitors(competition.CompetitionID).Any())
                {
                    // Competitor doesn't exist, then
                    // update competition record to database
                    competitionContext.Update(competition);
                }
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the view
                //to display error message
                return View(competition);
            }
        }

        // GET: AreaInterestController/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            Competition competition = competitionContext.GetDetails(id.Value);
            if (competition == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(competition);
        }

        // POST: AreaInterestController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Competition competition)
        {
            if (!competitionContext.GetSelectedCompetitionJudges(competition.CompetitionID).Any() && (!competitionContext.GetCompetitors(competition.CompetitionID).Any()))
            {
                // Competitor and CompetitionJudge doesn't exist, then
                // delete the competition record from database
                competitionContext.Delete(competition.CompetitionID);
            }
            // else return to index page
            return RedirectToAction("Index");
        }
        private List<SelectListItem> GetAreaInterests()
        {
            List<SelectListItem> aoi = new List<SelectListItem>();
            foreach (AreaInterest areaInterest in areaInterestContext.GetAllAreaInterests())
            {
                aoi.Add(new SelectListItem
                {
                    Value = areaInterest.AreaInterestID.ToString(),
                    Text = areaInterest.Name
                });
            }
            return aoi;
        }
        private List<AreaInterest> GetAllAreaInterests()
        {
            // Get a list of branches from database
            List<AreaInterest> aoiList = areaInterestContext.GetAllAreaInterests();
            // Adding a select prompt at the first row of the branch list
            aoiList.Insert(0, new AreaInterest
            {
                AreaInterestID = 0,
                Name = "--Select--"
            });
            return aoiList;
        }
        public List<Judges> GetJudges()
        {
            List<Judges> judgeList = new List<Judges>();
            foreach (Judge judge in judgeContext.GetAllJudges())
            {
                judgeList.Add(new Judges
                {
                    Name = judge.Name,
                    Selected = false
                });
            }
            return judgeList;
        }
    }
}
