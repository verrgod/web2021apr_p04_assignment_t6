﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P04_T6.Models
{
    public class Competitor
    {
        public int CompetitorID { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please enter a name!")]
        [StringLength(50, ErrorMessage = "Name cannot exceed 50 characters")]
        public string CompetitorName { get; set; }
        //change gender string to char after reviewal from teacher
        [Display(Name = "Salutation")]
        public string Salutation { get; set; }

        [Display(Name = "Email")]
        [EmailAddress]
        [CompetitorValidateEmailExists]
        public string EmailAddr { get; set; }


        [Display(Name = "Password")]
        [Required]
        public string Password { get; set; }
    }
}