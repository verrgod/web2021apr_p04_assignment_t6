﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P04_T6.Models
{
    public class JoinCompetition
    {
        public int CompetitionID { get; set; }
        public string CompetitionName { get; set; }

        public DateTime StartDate { get; set; }
    }
}