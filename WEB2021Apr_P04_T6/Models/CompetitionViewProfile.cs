﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P04_T6.Models
{
    public class CompetitionViewProfile
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please enter a name!")]
        [StringLength(50, ErrorMessage = "Name cannot exceed 50 characters")]
        public string name { get; set; }

        [Display(Name = "Gender")]
        public char gender { get; set; }

        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime? dob { get; set; }

        [Display(Name = "Address")]
        public string address { get; set; }

        [Display(Name = "Email")]
        [EmailAddress]
        [ValidateEmailExists]
        public string email { get; set; }

        [Display(Name = "Phone number")]
        public string phone_num { get; set; }

        [Display(Name = "Password")]
        [Required]
        public string Password { get; set; }
    }
}

