﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P04_T6.Models
{
    public class CompetitionSubmission
    {
        public int CompetitionID { get; set; }

        public int CompetitorID { get; set; }

        public string FileSubmitted { get; set; }

        //[DataType(DataType.Date)]
        //public DateTime? DateTimeFileUpload { get; set; }

        public string Appeal { get; set; }

        public int VoteCount { get; set; }

        public int? Ranking { get; set; }

        /*public String GetCompetitorName(int competitorID)
        {

        }*/
    }
}
