﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P04_T6.Models
{
    public class CompetitionJudge
    {
        public int CompetitionID { get; set; }
        public int JudgeID { get; set; }
    }
}
