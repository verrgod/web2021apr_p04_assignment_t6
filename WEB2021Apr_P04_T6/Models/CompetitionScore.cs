﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WEB2021Apr_P04_T6.Models
{
    public class CompetitionScore
    {
        public int CriteriaID { get; set; }

        public int CompetitorID { get; set; }

        public int CompetitionID { get; set; }

        [Range(0, 10, ErrorMessage =
        "Invalid value! Please enter a value from 0 to 10")]
        public int Score { get; set; }

        public CompetitionScore() { }

        public CompetitionScore (int comp, int id)
        {
            CompetitionID = comp;
            CompetitorID = id;
        }
    }
}
