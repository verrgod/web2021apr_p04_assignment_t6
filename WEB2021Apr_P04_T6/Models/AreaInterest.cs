﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P04_T6.Models
{
    public class AreaInterest
    {
        public int AreaInterestID { get; set; }
        [Required]
        [Display(Name = "Area of Interest")]
        [StringLength(50)]
        [ValidateAOIExists]
        public string Name { get; set; }
    }
}
