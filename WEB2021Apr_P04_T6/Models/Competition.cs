﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P04_T6.Models
{
    public class Competition
    {
        public int CompetitionID { get; set; }
        [Required]
        [Display(Name = "Area of Interest")]
        public int AreaInterestID { get; set; }
        [Required]
        [Display(Name = "Competition Name")]
        [StringLength(255, ErrorMessage = "Name cannot exceed 255 characters.")]
        public string CompetitionName { get; set; }
        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        // checking if end date is later than start date
        [ValidateDate("StartDate")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime EndDate { get; set; }
        [Display(Name = "Results Date")]
        // checking if results date is later than end date
        [ValidateDate("EndDate")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime ResultReleasedDate { get; set; }
        [Display(Name = "Judges")]
        // judges displayed below as checkboxes for user to select
        public List<Judges> Judges { get; set; }
    }

    // this is used for adding of judges when creating competitions
    public class Judges
    {
        public string Name { get; set; }
        public bool Selected { get; set; }
    }

    public class CompetitionViewModel
    {
        public int CompetitionID { get; set; }

        public DateTime StartDate { get; set; }

        public string CompetitionName { get; set; }

        public int? CompetitorID { get; set; }
        public string FileSubmitted { get; set; }


    }


    public class CompetitionScoreViewModel
    {

        public string CompetitionName { get; set; }

        public int Score { get; set; }


    }
}
