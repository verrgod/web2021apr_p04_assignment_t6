﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WEB2021Apr_P04_T6.Models
{
    public class Judge
    {
        public int JudgeId { get; set; }

        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        public string Salutation { get; set; }

        [Display(Name = "Email Address")]
        [Required]
        [EmailAddress]
        [ValidateEmailExists]
        public string Email { get; set; }

        [Display(Name = "Password")]
        [Required]
        public string Password { get; set; }

        [Display(Name = "Area of Interest")]
        [Required]
        public int AOI { get; set; }
    }

    public class Criteria
    {
        public int CriteriaID { get; set; }

        public int CompetitionID { get; set; }

        [Display(Name = "Criteria Name")]
        public string CriteriaName { get; set; }

        [Display(Name = "Weightage")]
        public int Weightage { get; set; }
    }
}
