﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P04_T6.Models
{
    public class SubmissionScoreViewModel
    {
        public List<CompetitionScore> scoreList { get; set; }
        public List<CompetitionSubmission> submissionList { get; set; }

        public SubmissionScoreViewModel()
        {
            scoreList = new List<CompetitionScore>();
            submissionList = new List<CompetitionSubmission>();
        }
    }
}
