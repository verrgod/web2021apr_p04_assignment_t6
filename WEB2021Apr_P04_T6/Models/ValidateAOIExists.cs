﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WEB2021Apr_P04_T6.DAL;

namespace WEB2021Apr_P04_T6.Models
{
    public class ValidateAOIExists : ValidationAttribute
    {
        private AreaInterestDAL aoiContext = new AreaInterestDAL();
        protected override ValidationResult IsValid(
        object value, ValidationContext validationContext)
        {
            // Get the email value to validate
            string name = Convert.ToString(value);
            // Casting the validation context to the "AreaInterest" model class
            AreaInterest aoi = (AreaInterest)validationContext.ObjectInstance;
            // Get the AreaInterestID from the AreaInterest instance
            int areaInterestID = aoi.AreaInterestID;
            if (aoiContext.IsAOIExist(name, areaInterestID))
                // validation failed
                return new ValidationResult
                ("Area of interest already exists!");
            else
                // validation passed
                return ValidationResult.Success;
        }
    }
}
