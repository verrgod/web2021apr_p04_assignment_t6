﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WEB2021Apr_P04_T6.Models;

namespace WEB2021Apr_P04_T6.DAL
{
    public class AreaInterestDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        //Constructor
        public AreaInterestDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "DatabaseConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }
        // getting every area interest
        public List<AreaInterest> GetAllAreaInterests()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all branches
            cmd.CommandText = @"SELECT * FROM AreaInterest";
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a branch list
            List<AreaInterest> areaInterestList = new List<AreaInterest>();
            while (reader.Read())
            {
                areaInterestList.Add(
                new AreaInterest
                {
                    AreaInterestID = reader.GetInt32(0), // 0 - 1st column
                    Name = reader.GetString(1), // 1 - 2nd column
                }
               );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return areaInterestList;
        }
        // getting all competitions of selected area interest
        public List<Competition> GetAllCompetitions(int areaInterestID)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all branches
            cmd.CommandText = @"SELECT * FROM Competition WHERE AreaInterestID = @selectedAreaInterestID";
            cmd.Parameters.AddWithValue("@selectedAreaInterestID", areaInterestID);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a branch list
            List<Competition> competitionList = new List<Competition>();
            while (reader.Read())
            {
                competitionList.Add(
                new Competition
                {
                    CompetitionID = reader.GetInt32(0),
                    AreaInterestID = reader.GetInt32(1),
                    CompetitionName = reader.GetString(2),
                    StartDate = reader.GetDateTime(3),
                    EndDate = reader.GetDateTime(4),
                    ResultReleasedDate = reader.GetDateTime(5)
                }
               );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return competitionList;
        }
        // getting details for selected area interest to delete
        public AreaInterest GetDetails(int areaInterestID)
        {
            AreaInterest aoi = new AreaInterest();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT * FROM AreaInterest
 WHERE AreaInterestID = @selectedAreaInterestID";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “staffId”.
            cmd.Parameters.AddWithValue("@selectedAreaInterestID", areaInterestID);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill staff object with values from the data reader
                    aoi.AreaInterestID = areaInterestID;
                    aoi.Name = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();
            return aoi;
        }
        // adding area interest
        public int Add(AreaInterest areaInterest)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO AreaInterest (Name)
OUTPUT INSERTED.AreaInterestID
VALUES(@name)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@name", areaInterest.Name);
            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            areaInterest.AreaInterestID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return areaInterest.AreaInterestID;
        }
        // checking to see if area interest of same name already exists
        public bool IsAOIExist(string name, int areaInterestID)
        {
            bool aoiFound = false;
            //Create a SqlCommand object and specify the SQL statement
            //to get a areaInterest record with the same name to be validated
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT AreaInterestID FROM AreaInterest
 WHERE Name=@selectedName";
            cmd.Parameters.AddWithValue("@selectedName", name);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetInt32(0) != areaInterestID)
                        //The area interest exists
                        aoiFound = true;
                }
            }
            else
            { //No record
                aoiFound = false; // area interest given with the same name does not exist
            }
            reader.Close();
            conn.Close();

            return aoiFound;
        }
        // deleting area interest record from database
        public int Delete(int areaInterestID)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM AreaInterest
 WHERE AreaInterestID = @selectedAreaInterestID";
            cmd.Parameters.AddWithValue("@selectedAreaInterestID", areaInterestID);
            //Open a database connection
            conn.Open();
            int rowAffected = 0;
            //Execute the DELETE SQL to remove the areaInterest record
            rowAffected += cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();
            //Return number of row of areaInterest record updated or deleted
            return rowAffected;
        }
    }
}

