﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P04_T6.Models;

namespace WEB2021Apr_P04_T6.DAL
{
    public class JoinCompetitionDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        //Constructor
        public JoinCompetitionDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "DatabaseConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }
        public JoinCompetition GetDetails(int competitionID)
        {
            JoinCompetition com = new JoinCompetition();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT CompetitionID, CompetitionName, StartDate * FROM Competition
 WHERE CompetitionID = @selectedCompetitionID";
            //Define the parameter used in SQL statement, value for the

            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionID);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    com.CompetitionID = competitionID;
                    com.CompetitionName = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                    com.StartDate = reader.GetDateTime(2);
                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();
            return com;
        }



    }
}