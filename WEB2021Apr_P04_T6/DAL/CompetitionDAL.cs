﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WEB2021Apr_P04_T6.Models;

namespace WEB2021Apr_P04_T6.DAL
{
    public class CompetitionDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        //Constructor
        public CompetitionDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "DatabaseConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }
        // getting every competitionjudge
        public List<CompetitionJudge> GetCompetitionJudges()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all branches
            cmd.CommandText = @"SELECT * FROM CompetitionJudge";
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a branch list
            List<CompetitionJudge> competitionJudgeList = new List<CompetitionJudge>();
            while (reader.Read())
            {
                competitionJudgeList.Add(
                    new CompetitionJudge
                    {
                        CompetitionID = reader.GetInt32(0),
                        JudgeID = reader.GetInt32(1)
                    }
               );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return competitionJudgeList;
        }
        // getting every competitionjudge from selected competition to edit/unassign
        public List<CompetitionJudge> GetSelectedCompetitionJudges(int competitionID)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all branches
            cmd.CommandText = @"SELECT * FROM CompetitionJudge WHERE CompetitionID = @selectedCompetitionID";
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionID);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a branch list
            List<CompetitionJudge> competitionJudgeList = new List<CompetitionJudge>();
            while (reader.Read())
            {
                competitionJudgeList.Add(
                    new CompetitionJudge
                    {
                        CompetitionID = reader.GetInt32(0),
                        JudgeID = reader.GetInt32(1)
                    }
               );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return competitionJudgeList;
        }
        // check for competitor submission so that admin can update/delete
        public List<Competitor> GetCompetitors(int competitionID)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all branches
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission WHERE CompetitionID = @selectedCompetitionID";
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionID);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a branch list
            List<Competitor> competitorList = new List<Competitor>();
            while (reader.Read())
            {
                competitorList.Add(
                    new Competitor
                    {
                        CompetitorID = reader.GetInt32(0),
                    }
               );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return competitorList;
        }
        // getting every competition
        public List<Competition> GetAllCompetitions()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all branches
            cmd.CommandText = @"SELECT * FROM Competition";
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a branch list
            List<Competition> competitionList = new List<Competition>();
            while (reader.Read())
            {
                competitionList.Add(
                new Competition
                {
                    CompetitionID = reader.GetInt32(0),
                    AreaInterestID = reader.GetInt32(1),
                    CompetitionName = reader.GetString(2),
                    StartDate = reader.GetDateTime(3),
                    EndDate = reader.GetDateTime(4),
                    ResultReleasedDate = reader.GetDateTime(5)
                }
               );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return competitionList;
        }
        // adding competition into database
        public int AddCompetition(Competition competition)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO Competition (AreaInterestID, CompetitionName, StartDate, EndDate,
 ResultReleasedDate)
OUTPUT INSERTED.CompetitionID
VALUES(@areaInterestID, @competitionName, @startDate, @endDate,
@resultReleasedDate)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@areaInterestID", competition.AreaInterestID);
            cmd.Parameters.AddWithValue("@competitionName", competition.CompetitionName);
            cmd.Parameters.AddWithValue("@startDate", competition.StartDate);
            cmd.Parameters.AddWithValue("@endDate", competition.EndDate);
            cmd.Parameters.AddWithValue("@resultReleasedDate", competition.ResultReleasedDate);
            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            competition.CompetitionID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return competition.CompetitionID;
        }
        // adding competitionjudge record into database
        public int AddJudge(CompetitionJudge judge)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO CompetitionJudge (CompetitionID, JudgeID)
OUTPUT VALUES(@competitionID, @judgeID)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@competitionID", judge.CompetitionID);
            cmd.Parameters.AddWithValue("@judgeID", judge.JudgeID);
            //A connection to database must be opened before any operations made.
            conn.Open();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return judge.CompetitionID;
        }
        // getting selected competition details to update/delete
        public Competition GetDetails(int competitionID)
        {
            Competition com = new Competition();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT * FROM Competition
 WHERE CompetitionID = @selectedCompetitionID";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “competitionID”.
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionID);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    com.CompetitionID = competitionID;
                    com.AreaInterestID = reader.GetInt32(1);
                    com.CompetitionName = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                    com.StartDate = reader.GetDateTime(3);
                    com.EndDate = reader.GetDateTime(4);
                    com.ResultReleasedDate = reader.GetDateTime(5);
                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();
            return com;
        }
        // update competition record on database
        public int Update(Competition com)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an UPDATE SQL statement
            cmd.CommandText = @"UPDATE Competition SET AreaInterestID=@areaInterestID, CompetitionName = @competitionName,
StartDate=@startDate, EndDate = @endDate, ResultReleasedDate = @resultReleasedDate
WHERE CompetitionID = @selectedCompetitionID";
            cmd.Parameters.AddWithValue("@areaInterestID", com.AreaInterestID);
            cmd.Parameters.AddWithValue("@competitionName", com.CompetitionName);
            cmd.Parameters.AddWithValue("@startDate", com.StartDate);
            cmd.Parameters.AddWithValue("@endDate", com.EndDate);
            cmd.Parameters.AddWithValue("@resultReleasedDate", com.ResultReleasedDate);
            cmd.Parameters.AddWithValue("@selectedCompetitionID", com.CompetitionID);
            //Open a database connection
            conn.Open();
            // declare variable for if statement
            int count = 0;
            // check if user chooses --select-- option in dropdown list
            if (com.AreaInterestID > 0)
            {
                // if user chooses --select-- option
                // perform edit task
                count = cmd.ExecuteNonQuery();
                // return count
                return count;
            }
            // otherwise, just close the database connection
            // and return 0
            conn.Close();
            return count;
        }
        // delete competition record from database
        public int Delete(int competitionID)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement
            //to delete a competition record specified by a competitionID
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM Competition
 WHERE CompetitionID = @selectedCompetitionID";
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionID);
            //Open a database connection
            conn.Open();
            int rowAffected = 0;
            //Execute the DELETE SQL to remove the competition record
            rowAffected += cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();
            //Return number of row of competition record updated or deleted
            return rowAffected;
        }
    }
}

