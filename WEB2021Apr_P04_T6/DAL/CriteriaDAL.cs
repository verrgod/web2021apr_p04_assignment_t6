﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WEB2021Apr_P04_T6.Models;

namespace WEB2021Apr_P04_T6.DAL
{
    public class CriteriaDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        //Constructor
        public CriteriaDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "DatabaseConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }
        public List<Criteria> GetAllCriteria()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all branches
            cmd.CommandText = @"SELECT * FROM Criteria";
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a branch list
            List<Criteria> criteriaList = new List<Criteria>();
            while (reader.Read())
            {
                criteriaList.Add(
                new Criteria
                {
                    CriteriaID = reader.GetInt32(0),
                    CompetitionID = reader.GetInt32(1),
                    CriteriaName = reader.GetString(2),
                    Weightage = reader.GetInt32(3)
                }
               );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return criteriaList;
        }
        public int Add(Criteria criteria)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated CriteriaID after insertion
            cmd.CommandText = @"INSERT INTO Criteria (CompetitionID, CriteriaName, Weightage)
OUTPUT INSERTED.CriteriaID
VALUES(@competitionID, @criteriaName, @weightage)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@competitionID", criteria.CompetitionID);
            cmd.Parameters.AddWithValue("@criteriaName", criteria.CriteriaName);
            cmd.Parameters.AddWithValue("@weightage", criteria.Weightage);
            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //CriteriaID after executing the INSERT SQL statement
            criteria.CriteriaID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return criteria.CriteriaID;
        }
        public Criteria GetDetails(int criteriaID)
        {
            Criteria criteria = new Criteria();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT * FROM Criteria
 WHERE CriteriaID = @selectedCriteriaID";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “criteriaID”.
            cmd.Parameters.AddWithValue("@selectedCriteriaID", criteriaID);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    criteria.CriteriaID = criteriaID;
                    criteria.CompetitionID = reader.GetInt32(1);
                    criteria.CriteriaName = reader.GetString(2);
                    criteria.Weightage = reader.GetInt32(3);
                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();
            return criteria;
        }
        // Return number of row updated
        public int Update(Criteria criteria)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an UPDATE SQL statement
            cmd.CommandText = @"UPDATE Criteria SET CriteriaName = @criteriaName,
Weightage = @weightage WHERE CriteriaID = @selectedCriteriaID AND CompetitionID = @selectedCompetitionID";
            cmd.Parameters.AddWithValue("@criteriaName", criteria.CriteriaName);
            cmd.Parameters.AddWithValue("@weightage", criteria.Weightage);
            cmd.Parameters.AddWithValue("@selectedCriteriaID", criteria.CriteriaID);
            cmd.Parameters.AddWithValue("@selectedCompetitionID", criteria.CompetitionID);
            //Open a database connection
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //Close the database connection
            conn.Close();
            return count;
        }
        public int Delete(int criteriaID, int competitionID)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement
            //to delete a criteria record specified by a Criteria ID
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM Criteria WHERE CriteriaID = @selectedCriteriaID";
            cmd.Parameters.AddWithValue("@selectedCriteriaID", criteriaID);
            //Check command
            SqlCommand checkCmd = conn.CreateCommand();
            checkCmd.CommandText = @"SELECT COUNT(*) FROM CompetitionScore 
WHERE CriteriaID = @selectedCriteriaID AND CompetitionID = @selectedCompetitionID";
            checkCmd.Parameters.AddWithValue("@selectedCriteriaID", criteriaID);
            checkCmd.Parameters.AddWithValue("@selectedCompetitionID", competitionID);
            //Open a database connection
            conn.Open();
            int rowAffected = 0;
            int checkRowAffected = 0;
            //Execute the DELETE SQL to remove the criteria record
            checkRowAffected += checkCmd.ExecuteNonQuery();
            if (checkRowAffected == 0)
            {
                rowAffected += cmd.ExecuteNonQuery();
                //Close database connection
                conn.Close();
                //Return number of row of criteria record updated or deleted
                return rowAffected;
            }
            else
            {
                return 0;
            }
        }
    }
}

