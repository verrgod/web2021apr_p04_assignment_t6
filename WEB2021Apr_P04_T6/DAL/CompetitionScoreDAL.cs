﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P04_T6.Models;

namespace WEB2021Apr_P04_T6.DAL
{
    public class CompetitionScoreDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public CompetitionScoreDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "DatabaseConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }
        //Get all competition submissions with file submitted
        public List<CompetitionSubmission> GetCompetitionSubmissions()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all branches
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission";
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a branch list
            List<CompetitionSubmission> competitionSubmissionList = new List<CompetitionSubmission>();
            while (reader.Read())
            {
                if (reader.IsDBNull(2) == false)
                {
                    competitionSubmissionList.Add(
                    new CompetitionSubmission
                    {
                        CompetitionID = reader.GetInt32(0),
                        CompetitorID = reader.GetInt32(1),
                        FileSubmitted = reader.GetString(2),
                        Appeal = !reader.IsDBNull(4) ? reader.GetString(4) : (string)null,
                        VoteCount = reader.GetInt32(5),
                        //Ranking = !reader.IsDBNull(6) ? reader.GetInt32(6) : (int?)null
                    }
                   );
                }
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return competitionSubmissionList;
        }
        //Get all competition scores recorded in Database
        public List<CompetitionScore> GetCompetitionScores()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all branches
            cmd.CommandText = @"SELECT * FROM CompetitionScore";
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a branch list
            List<CompetitionScore> competitionScoreList = new List<CompetitionScore>();
            while (reader.Read())
            {
                competitionScoreList.Add(
                new CompetitionScore
                {
                    CriteriaID = reader.GetInt32(0),
                    CompetitorID = reader.GetInt32(1),
                    CompetitionID = reader.GetInt32(2),
                    Score = reader.GetInt32(3)
                }
               );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return competitionScoreList;
        }

        public int Add(CompetitionScore competitionScore)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO CompetitionScore (CriteriaID, CompetitorID, CompetitionID, Score)
                                OUTPUT INSERTED.CompetitorID
                                VALUES(@criteriaID, @competitorID, @competitionID, @score)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@criteriaID", competitionScore.CriteriaID);
            cmd.Parameters.AddWithValue("@competitorID", competitionScore.CompetitorID);
            cmd.Parameters.AddWithValue("@competitionID", competitionScore.CompetitionID);
            cmd.Parameters.AddWithValue("@score", competitionScore.Score);
            //A connection to database must be opened before any operations made.
            conn.Open();
            int count = cmd.ExecuteNonQuery();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return count;
        }
        public int Update(CompetitionScore competitionScore)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an UPDATE SQL statement
            cmd.CommandText = @"UPDATE CompetitionScore SET Score = @score
WHERE CriteriaID = @selectedCriteriaID AND CompetitionID = @selectedCompetitionID AND CompetitorID = @selectedCompetitorID";
            cmd.Parameters.AddWithValue("@score", competitionScore.Score);
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitionScore.CompetitorID);
            cmd.Parameters.AddWithValue("@selectedCriteriaID", competitionScore.CriteriaID);
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionScore.CompetitionID);
            //Open a database connection
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //Close the database connection
            conn.Close();
            return count;
        }
        //Get subimission details of specific Competitor for a competition
        public CompetitionSubmission GetSubmissionDetails(int competitionID, int competitorID)
        {
            CompetitionSubmission competitionSubmission = new CompetitionSubmission();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission
 WHERE CompetitionID = @selectedCompetitionID AND CompetitorID = @selectedCompetitorID";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “”.
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionID);
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitorID);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    competitionSubmission.CompetitionID = reader.GetInt32(0);
                    competitionSubmission.CompetitorID = reader.GetInt32(1);
                    competitionSubmission.FileSubmitted = reader.GetString(2);
                    competitionSubmission.Appeal = !reader.IsDBNull(4) ? reader.GetString(4) : (string)null;
                    competitionSubmission.VoteCount = reader.GetInt32(5);
                    //competitionSubmission.Ranking = !reader.IsDBNull(6) ? reader.GetInt32(6) : (int?)null
                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();
            return competitionSubmission;
        }
        //Get all scores for a specific Competitor
        public List<CompetitionScore> GetCompetitorScores(int competitionID, int competitorID)
        {
            List<CompetitionScore> competitionScoreList = new List<CompetitionScore>();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT * FROM CompetitionScore
 WHERE CompetitionID = @selectedCompetitionID AND CompetitorID = @selectedCompetitorID";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “”.
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionID);
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitorID);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                competitionScoreList.Add(
                    new CompetitionScore
                    {
                        CriteriaID = reader.GetInt32(0),
                        CompetitorID = reader.GetInt32(1),
                        CompetitionID = reader.GetInt32(2),
                        Score = reader.GetInt32(3)
                    }
                );
            }
            
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();
            return competitionScoreList;
        }
        //Get score details for a specific Competitor and specific criteria for a competition
        public CompetitionScore GetCompetitorScoreDetails(int competitionID, int competitorID, int criteriaID)
        {
            CompetitionScore competitionScore = new CompetitionScore();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT * FROM CompetitionScore
 WHERE CompetitionID = @selectedCompetitionID AND CompetitorID = @selectedCompetitorID AND CriteriaID = @selectedCriteriaID";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “”.
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionID);
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitorID);
            cmd.Parameters.AddWithValue("@selectedCriteriaID", criteriaID);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    competitionScore.CriteriaID = criteriaID;
                    competitionScore.CompetitorID = competitorID;
                    competitionScore.CompetitionID = competitionID;
                    competitionScore.Score = reader.GetInt32(3);
                }
            }

            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();
            return competitionScore;
        }
    }
}
