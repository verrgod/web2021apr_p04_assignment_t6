﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WEB2021Apr_P04_T6.Models;

namespace WEB2021Apr_P04_T6.DAL
{

    public class CompetitorDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public CompetitorDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "DatabaseConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public bool IsEmailExist(string email, int Competitor)
        {
            conn.Close();
            bool emailFound = false;
            //Create a SqlCommand object and specify the SQL statement
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Competitor WHERE EmailAddr=@selectedEmail";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetInt32(0) != Competitor)
                        //The email address is used by another staff
                        emailFound = true;
                }
            }
            else
            { //No record
                emailFound = false; // The email address given does not exist
            }
            reader.Close();
            conn.Close();

            return emailFound;
        }
        public List<CompetitionViewModel> GetAllCompetitionList(int competitorId)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select certain branches
            cmd.CommandText = @"SELECT c.CompetitionID, CompetitionName, StartDate, cs.CompetitorID,cs.FileSubmitted  FROM Competition c LEFT JOIN CompetitionSubmission cs ON c.CompetitionID = cs.CompetitionID AND cs.CompetitorID ='" + competitorId + "'";
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a branch list
            List<CompetitionViewModel> competitionList = new List<CompetitionViewModel>();

            while (reader.Read())
            {
                competitionList.Add(
                    new CompetitionViewModel
                    {
                        CompetitionID = reader.GetInt32(0),
                        CompetitionName = reader.GetString(1),
                        StartDate = reader.GetDateTime(2),
                        CompetitorID = reader.IsDBNull(3) ? 0 : (int)reader.GetInt32(3) as int? ?? default(int),
                        FileSubmitted = reader.IsDBNull(4) ? "" : (string)reader.GetString(4) as string ?? default(string),

                    }
                    );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return competitionList;
        }

        public bool IsUser(string loginID, string password)
        {
            conn.Close();
            bool isUser = false;
            //Create a SqlCommand object and specify the SQL statement
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Competitor WHERE EmailAddr=@selectedEmail AND Password=@selectedPassword";
            cmd.Parameters.AddWithValue("@selectedEmail", loginID);
            cmd.Parameters.AddWithValue("@selectedPassword", password);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetString(3) == loginID && reader.GetString(4) == password)
                        //The user address is used by another staff
                        isUser = true;
                }
            }
            else
            { //No record
                isUser = false; // The user address given does not exist
            }
            reader.Close();
            conn.Close();

            return isUser;
        }

        public List<CompetitionScoreViewModel> GetAllCompetitionScoreList(int competitorId)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select certain branches
            cmd.CommandText = @"SELECT  CompetitionName, cs.Score  FROM Competition c Inner JOIN CompetitionScore cs ON c.CompetitionID = cs.CompetitionID AND cs.CompetitorID ='" + competitorId + "'";
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a branch list
            List<CompetitionScoreViewModel> competitionList = new List<CompetitionScoreViewModel>();

            while (reader.Read())
            {
                competitionList.Add(
                    new CompetitionScoreViewModel
                    {
                        CompetitionName = reader.GetString(0),
                        Score = reader.IsDBNull(1) ? 0 : (int)reader.GetInt32(1) as int? ?? default(int),

                    }
                    );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return competitionList;
        }

        public int Add(Competitor competitor)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO Competitor (CompetitorName, Salutation, EmailAddr, Password)
                                OUTPUT INSERTED.CompetitorID
                                VALUES(@name, @salutation, @email, @password)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@name", competitor.CompetitorName);
            cmd.Parameters.AddWithValue("@salutation", competitor.Salutation);
            cmd.Parameters.AddWithValue("@email", competitor.EmailAddr);
            cmd.Parameters.AddWithValue("@password", competitor.Password);
            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            competitor.CompetitorID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return competitor.CompetitorID;
        }


        public int GetCompetitorId(string loginId)
        {
            conn.Close();
            int CompetitorId = 0;
            //Create a SqlCommand object and specify the SQL statement
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Competitor WHERE EmailAddr=@selectedEmail";
            cmd.Parameters.AddWithValue("@selectedEmail", loginId);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    CompetitorId = reader.GetInt32(0);
                }
            }

            reader.Close();
            conn.Close();

            return CompetitorId;
        }


        public int Join(int competitionid, int competitorId, int voteCount)
        {
            conn.Close();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO CompetitionSubmission (CompetitionID,CompetitorID,VoteCount)
                                OUTPUT INSERTED.CompetitionID
                                VALUES(@CompetitionID,@CompetitorID,@VoteCount)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@CompetitionID", competitionid);
            cmd.Parameters.AddWithValue("@CompetitorID", competitorId);
            cmd.Parameters.AddWithValue("@VoteCount", voteCount);
            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            int CompetitionID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return CompetitionID;
        }

        public bool uploadfile(int competitionid, int competitorId, string filename)
        {
            //Create a SqlCommand object from connection object
            conn.Open();
            SqlCommand comm;
            var querry = "update CompetitionSubmission set FileSubmitted= '" + filename + "', DateTimeFileUpload= '" + System.DateTime.Now + "' where CompetitionID ='" + competitionid + "'  AND competitorId ='" + competitorId + "' ";
            comm = new SqlCommand(querry, conn);
            try
            {
                comm.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                conn.Close();
            }
        }


    }
}